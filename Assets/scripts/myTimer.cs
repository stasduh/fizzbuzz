﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class myTimer : MonoBehaviour {
	private float systemTimer;
	public static int screenTimer;

	// Use this for initialization
	void Start () {
		systemTimer = 1.0f;
		screenTimer = 1;

	}

	// Update is called once per frame
	void Update () {
		systemTimer = Time.timeSinceLevelLoad;    

		screenTimer = (int)systemTimer;
		if (UIController.levelForChildren == true) {//проверяет булево из другого класса UIGameController
			screenTimer /= 2; //уменьшение скорости, уровень для детей
			GameObject.Find ("Increment").GetComponent<Text> ().text = screenTimer.ToString ();
			if (UIGameController.stopTheTimer == true) {//проверяет булево из другого класса UIGameController
				screenTimer = 0;//обнуляет таймер, который передается в UIGameController. Необходимо, чтобы в момент неправильного ответа остановить проверку на пассивность
			}
		} else {
			GameObject.Find ("Increment").GetComponent<Text> ().text = screenTimer.ToString ();
			if (UIGameController.stopTheTimer == true) {//проверяет булево из другого класса UIGameController
				screenTimer = 0;//обнуляет таймер, который передается в UIGameController. Необходимо, чтобы в момент неправильного ответа остановить проверку на пассивность
			}
		}
		//Time.timeScale = 0;//пауза в игре
		
		//Ниже 3 условия прописывал для отладки
		//if (screenTimer % 3 == 0) {     
		//	GameObject.Find ("Increment").GetComponent<Text> ().text = "Fizz";

		//}


		//if (screenTimer % 5 == 0) {
		//	GameObject.Find ("Increment").GetComponent<Text> ().text = "Buzz";
		//}
		//if (screenTimer % 3 == 0 && screenTimer % 5 == 0) {
		//	GameObject.Find ("Increment").GetComponent<Text> ().text = "FizzBuzz";
		//}
	}


}
