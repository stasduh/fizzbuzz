﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class UIController : MonoBehaviour {
	public Animator aboutGameButton; 
	public Animator panelAboutGame;
	public Animator settingsButton; 
	public Animator panelSettings;
	public static bool levelForChildren;

	void Start () {
		levelForChildren = false;
	} 

	// About game open 
	public void TouchAboutGameButton (){
		panelAboutGame.enabled = true;
		panelAboutGame.SetBool ("visible", false);
	}
	// About game close 
	public void ClosePanelAboutGame(){
		panelAboutGame.SetBool ("visible", true);
	}
	// Settings open 
	public void TouchSettingsButton (){
		panelSettings.enabled = true;
		panelSettings.SetBool ("visible", false);
	}
	// Settings close
	public void ClosePanelSettings(){
		panelSettings.SetBool ("visible", true);
	}
	// Нажатие кнопки Start
	public void TouchStartButton(){
		Application.LoadLevel("Game");
	}
	// Нажатие кнопки Quit
	public void TouchQuitButton(){
		Application.Quit ();
	}
	// Нажатие кнопки levelForChildren
	public void TouchToggleLevelChild(){
		if (levelForChildren == false) {
			levelForChildren = true;
		} else {
			levelForChildren = false;
		}
	}
}
