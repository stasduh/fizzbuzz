﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class UIGameController : MonoBehaviour {
	public int incrementCounter; //переменная получает значение счетчика(таймера) из Mytimer.cs
	public GameObject GameMessage;//приложение работает и без этой строки. С этой строкой компилятор не выдает предупреждение
	public Animator GameOverPanel; //аниматор панели, которая появляется при неправльном ответе
	public static bool stopTheTimer;//булево переменная необходима для передачи значения в класс myTimer, где сбрасывается таймер
	private int scoreFizz; //переменная участвует в проверке нажатия на кнопку Fizz. Пассивность игрока - это тоже неправильный ответ.
	private int scoreBuzz; //переменная участвует в проверке нажатия на кнопку Buzz.
	private int scoreFizzBuzz; //переменная участвует в проверке нажатия на кнопку FizzBuzz.
	private bool pauseButton;


	void Start () {
		stopTheTimer = false;
		scoreFizz = 3;
		scoreBuzz = 5;
		scoreFizzBuzz = 15;
		pauseButton = false;
	} 
	//______Нажатие на кнопку Fizz______
	public void TouchFizzButton (){
		
		incrementCounter =  (int)myTimer.screenTimer;// переменная incrementCounter получает значение счетчика
													        // в момент нажатия кнопки и далее сравнивает
		                                                    // делится на 3 и не делится на 5
		if (incrementCounter % 3 == 0 && incrementCounter % 5 != 0) {
			
			GameObject.Find ("GameMessage").GetComponent<Text> ().text = "True";//появляется текст "True" при правильном ответе 
			StartCoroutine("ClearGameMessage");//стирает текст "True" через 0.4 секунды. Код корутины см. ниже
			scoreFizz += 3; //увеличение на 3 при правильном ответе, чтобы проверка на пассивность на следующих секундах была false 
			Debug.Log ("Fizz Correctly"); //сообщение в консоль для отладки
			Debug.Log ("scoreFizz " + scoreFizz);
		} else {
			stopTheTimer = true;//в классе myTimer сработает нужный код. см. myTimer
			GameOverPanel.enabled = true; // При неправильном ответе появляется панель с кнопками Рестарт и Меню
			GameObject.Find ("GameOverMessage").GetComponent<Text> ().text = "Mistake! The number " + incrementCounter + " is not divisible by 3. You have reached the number " + (int)myTimer.screenTimer + ". Let's try to beat your score!";//Текст на панели с кнопками Рестарт и Меню, чтобы игрок понимал, где дал неправильный ответ
			Debug.Log ("Fizz Wrong"); //сообщение в консоль для отладки
		}

	}

	//______Нажатие на кнопку Buzz______
	public void TouchBuzzButton (){
		
		incrementCounter = (int)myTimer.screenTimer; // переменная incrementCounter получает значение счетчика
													        // в момент нажатия кнопки и далее сравнивает
													        // делится на 5 и не делится на 3
		if (incrementCounter % 5 == 0 && incrementCounter % 3 != 0) {
			
			GameObject.Find ("GameMessage").GetComponent<Text> ().text = "True";//появляется текст "True" при правильном ответе
			StartCoroutine("ClearGameMessage");//стирает текст "True" через 0.4 секунды. Код корутины см. ниже
			scoreBuzz += 5; //увеличение на 5 при правильном ответе, чтобы проверка на пассивность на следующих секундах была false 
			Debug.Log ("Buzz Correctly");
			Debug.Log ("scoreBuzz " + scoreBuzz);
		} else {
			stopTheTimer = true;//в классе myTimer сработает нужный код. см. myTimer
			GameOverPanel.enabled = true; // При неправильном ответе появляется панель с кнопками Рестарт и Меню
			GameObject.Find ("GameOverMessage").GetComponent<Text> ().text = "Mistake! The number " + incrementCounter + " is not divisible by 5. You have reached the number " + (int)myTimer.screenTimer + ". Let's try to beat your score!";
			Debug.Log ("Buzz Wrong");
		}
	}

	//______Нажатие на кнопку FizzBuzz______
	public void TouchFizzBuzzButton (){
		
		incrementCounter = (int)myTimer.screenTimer; // переменная incrementCounter получает значение счетчика
												            // в момент нажатия кнопки и далее сравнивает
												            // делится на 3 и делится на 5
		if (incrementCounter % 3 == 0 && incrementCounter % 5 == 0) {

			GameObject.Find ("GameMessage").GetComponent<Text> ().text = "True";//появляется текст "True" при правильном ответе 
			StartCoroutine("ClearGameMessage");//стирает текст "True" через 0.4 секунды. Код корутины см. ниже
			scoreFizzBuzz += 15; //увеличение на 15 при правильном ответе, чтобы проверка на пассивность на следующих секундах была false
			scoreFizz += 3; //тоже надо увеличить счетчик
			scoreBuzz += 5; 
			Debug.Log ("FizzBuzz Correctly");
			Debug.Log ("scoreFizzBuzz " + scoreFizzBuzz);
		} else {
			stopTheTimer = true;//в классе myTimer сработает нужный код. см. myTimer
			GameOverPanel.enabled = true;           // При неправильном ответе появляется панель с кнопками Рестарт и Меню
			GameObject.Find ("GameOverMessage").GetComponent<Text> ().text = "Mistake! The number " + incrementCounter + " is not divisible by 3 and 5. You have reached the number " + (int)myTimer.screenTimer + ". Let's try to beat your score!";

			Debug.Log ("FizzBuzz Wrong");
		}
	}

	void Update () {
		//______Пассивность игрока - это тоже, что и неправильный ответ_____
		//______Проверка на нажатие Fizz
		if (scoreFizz == ( (int)myTimer.screenTimer - 1 ) && !(scoreBuzz == ( (int)myTimer.screenTimer - 1)) ) {
					
			GameOverPanel.enabled = true;           //При пассивности появляется панель с кнопками Рестарт и Меню  
			GameObject.Find ("GameOverMessage").GetComponent<Text> ().text = "The passivity of the player. When was number " + scoreFizz + " you did not press Fizz. You have reached the number " + ((int)myTimer.screenTimer - 1) + ". Let's try to beat your score!";
			scoreFizz = 0;// сброс, иначе на следующей секунде происходит проверка на пассивность
			scoreBuzz = 0;// сброс, иначе на следующей секунде происходит проверка на пассивность
			scoreFizzBuzz = 0;

			}
		//______Проверка на нажатие Buzz
		if (scoreBuzz == ( (int)myTimer.screenTimer - 1 ) && !(scoreFizz == ( (int)myTimer.screenTimer - 1))) {

			GameOverPanel.enabled = true;           //При пассивности появляется панель с кнопками Рестарт и Меню  
			GameObject.Find ("GameOverMessage").GetComponent<Text> ().text = "The passivity of the player. When was number " + scoreBuzz + " you did not press Buzz. You have reached the number " + ((int)myTimer.screenTimer - 1) + ". Let's try to beat your score!";
			scoreFizz = 0;// сброс, иначе на следующей секунде происходит проверка на пассивность
			scoreBuzz = 0;// сброс, иначе на следующей секунде происходит проверка на пассивность
			scoreFizzBuzz = 0;

		}
		//______Проверка на нажатие FizzBuzz
		if (scoreFizzBuzz == ( (int)myTimer.screenTimer - 1 ) ) {

			GameOverPanel.enabled = true;           //При пассивности появляется панель с кнопками Рестарт и Меню  
			GameObject.Find ("GameOverMessage").GetComponent<Text> ().text = "The passivity of the player. When was number " + scoreFizzBuzz + " you did not press FizzBuzz. You have reached the number " + ((int)myTimer.screenTimer - 1) + ". Let's try to beat your score!";
			scoreFizz = 0;// сброс, иначе на следующей секунде происходит проверка на пассивность
			scoreBuzz = 0;// сброс, иначе на следующей секунде происходит проверка на пассивность
			scoreFizzBuzz = 0;

		}

	}

	//______Нажатие на кнопку Restart______
	public void TouchRestartButton (){
		Application.LoadLevel("Game");
	}
	//______Нажатие кнопки Menu______
	public void TouchMenuButton(){
		Application.LoadLevel("Scene");
	}

	//______После правильного ответа, через 0.4 секунды, Убирает текст "True" из текстового поля GameMessage_____
	IEnumerator ClearGameMessage(){
		yield return new WaitForSeconds (0.4f);
		GameObject.Find ("GameMessage").GetComponent<Text> ().text = " ";
	}
	//______Нажатие кнопки Pause______
	public void TouchPauseButton(){
		if (pauseButton == false) {
			Time.timeScale = 0;//пауза в игре
			pauseButton = true;//каждый раз при нажатии кнопки Pause булево значение меняется, 
								//соответственно чередуется выполнение условия if
		} else {
			Time.timeScale = 1;//продолжение игры
			pauseButton = false;
		}

	}

}